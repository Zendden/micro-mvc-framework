<?php

namespace app\core;

/**
 * Class Render, service.
 */
class Render {

    /**
     * Render class properties.
     * 
     * @var array  $path
     * @var string $controller
     * @var string $action
     * @var array $renderConf
     */
    public    $path          = null;
    public    $controller    = null;
    public    $action        = null;
    protected $renderConf    = null;

    /**
     * Required the configuration from ./config/render.conf.php
     * 
     * Exploding such path `main/index` to array $this->path = [0 => 'main', 1 => 'index'];
     */
    public function __construct () {
        $this->renderConf   = require_once ('app\config\render.conf.php');

        $uriParts           = explode ('/', $_GET['uri']);
        $this->controller   = !empty ($uriParts[0]) ? $uriParts[0] : $this->renderConf['defaultController'];
        $this->action       = !empty ($uriParts[1]) ? $uriParts[1] : $this->renderConf['defaultAction'];
    }

    /**
     * Render method responsible for extracting variable into `Layout`, 
     * also turning on the buffering output and unite templates into one layout. 
     * 
     * @param array $param
     * @return void
     */
    public function render (array $params = []): void {
        /**
         * Create the propertie {$this->path} here,
         * because this is the last step before rendering.
         * Method `setAnotherLayout` will not work, if using it in the contructor.
         * 
         * @var string $this->path
         */
        $this->path         = $this->renderConf['pathPrefix'].'\\'       //prefix of path to file
                             .strtolower ($this->controller).'\\'        //name of  controller to lowercase
                             .strtolower ($this->action)                 //name of `controller->*action*` to lowercase
                             .$this->renderConf['namingPostfix']         //the end prefix of naming template files
                             .$this->renderConf['defaultFileExtention']; //default extension of your project files `.php`


        /**
         * Checking parameters array on empty,
         * if not so, then extract() it.
         */
        !empty ($params) 
            ? extract ($params) 
            : null;

        /**
         * Checking file on exist, if so,
         * then:
         *      -starting buffering
         *      -including the target layout
         *      -getting the buffering output into target variable
         *      -including the base tempalte
         * if not so, die ();
         */
        if (file_exists ($this->path)) {
            ob_start ();
            include ($this->path);
            $content = ob_get_clean ();
            include (
                 $this->renderConf['pathPrefix'].'\\'
                .$this->renderConf['defaultLayout']
                .$this->renderConf['defaultFileExtention']
            );
        } else 
            die ('Template not found');
    }

    /**
     * Allows you to change the current layout to another with help.
     * 
     * ../views/{folder}/{file} 
     *      example  =>  folder/file  =>  {main}/{index}
     * 
     * as name of controllers without postfix `Controller` and
     * names of layouts without postfix `Layout`
     * 
     * @param string $controllerName
     * @param string $actionName
     * @return void
     */
    public function setAnotherLayout (string $controllerName, string $actionName): void {
        if (!empty ($controllerName) and $controllerName !== '' and !is_null ($controllerName)) {
            if (!empty ($actionName) and $actionName !== '' and !is_null ($actionName)) {
                $this->controller = strtolower ($controllerName);
                $this->action     = strtolower ($actionName);
            }
        }
    }
}