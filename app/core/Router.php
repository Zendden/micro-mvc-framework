<?php

namespace app\core;

/**
 * Class Router, service, pattern Singleton.
 */
class Router {

    /**
     * -Handler of self-class-instance.
     * -Regular expression pattern.
     * 
     * @var Router|null $instance
     * @var string $regExpr
     */
    protected static $instance = null;
    protected static $regExpr  = '/[(\^)(\*)(\')(\/)(\\\)(<)(>)(&)(|)]/';

    
    /**
     * Getting the only one instance of Router class.
     * 
     * @param void
     * @return \Router
     */
    public static function getInstance (): self {
        if (is_null (self::$instance)) self::$instance = new Router ();
        
        return self::$instance;
    }

    /**
     * Service method for getting URI-parts (domain.com/{first_uri_part}/{second_uri_part}?{farther params}).
     * 
     * @param void
     * @return array|null
     */
    protected static function prepareUriParts ():? array {
        $rawUriParts    = explode ('/', $_GET['uri']);
        $uriParts       = null;

        if (!empty ($rawUriParts)) {
            foreach ($rawUriParts as $key => $uriPart) {
                if (!empty ($uriPart)) $uriParts[preg_replace (self::$regExpr, '', $key)] = preg_replace (self::$regExpr, '', $uriPart);
            }
        }
        
        return $uriParts;
    }

    /**
     * Service method for getting params from the $_GET array.
     * 
     * @param void
     * @return array|null
     */
    protected static function prepareParams ():? array {
        $rawParams      = $_GET;
        $params         = null;

        if (isset ($rawParams['uri'])) unset ($rawParams['uri']);

        if (!empty ($rawParams)) {
            foreach ($rawParams as $key => $param) {
                if (!empty ($param)) $params[preg_replace (self::$regExpr, '', $key)] = preg_replace (self::$regExpr, '', $param);
            }
        }
        
        return $params;
    }

    /**
     * Getters of class properties.
     * 
     * -getUriParts () return an array with the name of controller and his method.
     * 
     * @param void
     * @return array|null
     */
    public static function getUriParts ():? array {
        return self::prepareUriParts ();
    }

    /**
     * -getParams () return an array with GET-parameters. 
     * 
     * @param void
     * @return array|null
     */
    public static function getParams ():? array {
        return self::prepareParams ();
    }

    /**
     * Method accept a number of uri-part, and ret urn this part.
     *  0 - controller 
     *  1 - action of the target controller
     * 
     * @param int $partNum
     * @return string|null
     */
    public static function getUriPartByNum (int $partNum):? string {
        $uriParts = self::getUriParts ();

        return $uriParts[$partNum];
    }

    /**
     * Method accepts names of GET-variables.
     * 
     * @param string $paramName
     * @return string|null
     */
    public static function getParamByName (string $paramName):? string {
        $params = self::getParams ();

        return $params[$paramName];
    }

    /**
     * Method adapter transmit the `REQUEST_URI`.
     * 
     * @param void
     * @return string 
     */
    public static function getUriString (): string {
        return $_SERVER['REQUEST_URI'];
    }

    /**
     * Closing the magic methods.
     */
    private function __construct () {}
    private function __clone () {}
    private function __wakeup () {}
}