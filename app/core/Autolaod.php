<?php

namespace app\core;

/**
 * $rawClassPath    = (example) app\src\controllers\PostController
 * $filePath        = (example) app/src/controllers/PostController.php
 */
spl_autoload_register (function (string $rawClassPath) {
    $filePath = $rawClassPath.'.php';
    
    if (file_exists ($filePath)) {
        require_once str_replace ('\\', '/', $filePath);
    } else 
        throw new \Exception (
            'File ['.$filePath.'] not found.', 
            500
        );
}); 
