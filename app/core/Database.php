<?php 

namespace app\core;

use PDO;

/**
 * Class Database, pattern Singleton.
 */
class Database {

    /**
     * Properties which need for configure PDO-class's constructor.
     * 
     * @var string $host
     * @var string $user
     * @var string $name
     * @var string $pass
     */
    private static $host = '';     //Host: localhost
    private static $user = '';     //User: username for entry into MySQL
    private static $name = '';     //Name: target database name
    private static $pass = '';     //Pass: password from MySQL, default = ''

    /**
     * Handler of Database|PDO instance.
     * 
     * @var \PDO $connection
     */
    private static $connection = null;

    /**
     * -Property, which responsible for configuration query-response from database.
     * 
     * @var array $pdoOptions
     */
    private static $pdoOptions = [
        PDO::ATTR_ERRMODE               => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE    => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES      => TRUE
    ];


    /**
     * Creating a new instance of PDO,
     * setting PDO config and forming the dsn string.
     * 
     * @param void
     * @return void
     */
    public static function createConnection (): void {
        /* Getting data for forming DSN string */
        $pdoConf = self::getConfig ();

        /* Applying configurations */
        self::$host = $pdoConf['host'];
        self::$user = $pdoConf['user'];
        self::$name = $pdoConf['name'];
        self::$pass = $pdoConf['pass'];

        /* Creating the DSN string */
        $dsn = self::getDSN ();

        /* Creating a new instance of PDO and assign it to handler propertie {$connection} */
        self::$connection = new PDO ($dsn, self::$user, self::$pass, self::$pdoOptions);
    }

    /**
     * Transfering the handler of PDO-instance.
     * 
     * @param void
     * @return \PDO
     */
    public static function getConnection (): \PDO {
        if (is_null (self::$connection)) {
            self::createConnection ();
        }

        return self::$connection;
    }

    /**
     * Static method for creating DSN string.
     * 
     * @param void
     * @return string
     */
    private static function getDSN (): string {
        $dsn = '';

        if (self::$host and self::$name !== '') {
            $dsn = "mysql:host=".self::$host.";dbname=".self::$name.";charset=UTF8;";
        } else {
            throw new \Exception (
                'Cannot get DSN string.', 
                500
            );
        }

        return $dsn;
    }

    /**
     * Static method for getting configurations.
     * 
     * @param void
     * @return array
     */
    private static function getConfig (): array {
        $pdoConf = [];
        
        if (file_exists ('app\config\database.conf.php')) {
            $pdoConf = require_once ('app\config\database.conf.php');
        } else {
            throw new \Exception (
                "Configuration file cannot be load.",
                500
            );
        }

        return $pdoConf;
    }
    
    /**
     * Closing connection
     * 
     * @param void
     * @return void
     */
    public static function closeConnection (): void {
        self::$connection = null;
    }

    /**
     * Closing the magic methods.
     */
    private function __construct () {}
    private function __clone () {}
    private function __wakeup () {}
}