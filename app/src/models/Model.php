<?php 

namespace app\src\models;

use app\core\Database;

abstract class Model {

    /**
     * Default property, which handle object of PDO into yourself.
     * 
     * @var \PDO|null $connection
     */
    public $connection = null;


    /**
     * Default constructor, if need call the constructor into the child-models,
     * type the `parent::__construct ();` into child-constructor.
     * 
     * @param void
     */
    public function __construct () {
        $this->connection = Database::getConnection ();
    }


    /**
     * FindAll method must get required parameter $targetTable,
     * others is not required.
     * 
     * @param string $targetTable
     * @param int $limitFrom
     * @param int $limitTo
     * @return array
     */
    public function findAll (string $targetTable, int $limitFrom = null, int $limitTo = null): array {
        if (!empty ($targetTable)) {
            $sql = "SELECT * FROM {$targetTable}";
            
            if (!is_null ($limitFrom)) $sql .= " LIMIT {$limitFrom}";
            if (!is_null ($limitTo)) $sql .= ", {$limitTo}";

            $query = $this->connection->prepare ($sql);
            $query->execute ();
        } else 
            throw new \Exception (
                "Target table is not clarified [passed table name = `{$targetTable}`]", 
                500
            );

            $result = $query->fetchAll ();
            return $result;
    }

    /**
     * FindBy method must get required parameters: 
     * $targetTable, $queryParams = ['fieldIntoDB' => 'valueWhichWantToFind'],
     * other is not required.
     * 
     * @param string $targetTable
     * @param array $queryParams
     * @param int $limitFrom
     * @param int $limitTo
     * @return array
     */
    public function findBy (string $targetTable, array $queryParams, int $limitFrom = null, int $limitTo = null): array {
        if (!empty ($targetTable) and !empty ($queryParams)) {
            $sql = "SELECT * FROM {$targetTable} WHERE ";
            
            if (!empty ($queryParams)) {
                /**
                 * Duty vars
                 * @var int $paramCounter
                 * @var int $paramNumber
                 */
                $paramCounter = 1;
                $paramNumber  = count ($queryParams);

                foreach ($queryParams as $field => $value) {
                    $sql .= "{$field} = :{$field}";

                    /**
                     * Writing `AND` each time after new pair $field = :field,
                     * into prepared statement.
                     */
                    if ($paramCounter !== $paramNumber) {
                        $sql .= " AND ";
                        $paramCounter++;
                    }
                }
            } else 
                throw new \Exception (
                    "No one parameter had been passed [".print_r ($queryParams)."]", 
                    500
                );

            if (!is_null ($limitFrom)) $sql .= " LIMIT {$limitFrom}";
            if (!is_null ($limitTo)) $sql .= ", {$limitTo}";

            $query = $this->connection->prepare ($sql);
            $query->execute ($queryParams);

        } else 
            throw new \Exception (
                "Target table is not clarified [passed table name = `{$targetTable}`]", 
                500
            );
        
        $result = $query->fetchAll ();
        return $result;
    }

    public function query (string $sql, array $params = [], string $fetchMethod = null) {
        $query = $this->connection->prepare ($sql);

        if (!empty ($params)) {
            foreach ($params as $paramName => $paramValue) {
                $paramType = is_int ($paramValue) ? \PDO::PARAM_INT : \PDO::PARAM_STR;

                $query->bindValue (":{$paramName}", $paramValue, $paramType);
            }
        }

        $query->execute ();

        if (is_null ($fetchMethod)) {
            return $query;
        } else {
            $result = $query->$fetchMethod ();
            return $result;
        }
            
    }
}