<?php

namespace app\src\models;

class UserModel extends Model {

    /**
     * Handler of validation errors.
     * 
     * @var string $error
     */
    public $error;

    /**
     * Validation method, take a two arrays as params, first containt fields and
     * second fields with values.
     * 
     * @param array $patternFields
     * @param array $inputFields
     * @return bool
     */
    public function validateUserInput (array $patternFields, array $inputFields): bool {
        $rules = [
            'login' =>  [
                'regEx' => '#^[a-zA-Z0-9]{5,30}$#',
                'error' => 'Login should have at least 5 and no more 30 symbols, only numbers and latin letters.'
            ],
            'email' => [
                'regEx' => '#^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1}))@([-A-Za-z0-9]{1,}\.){1,2}[-A-Za-z]{2,})$#u',
                'error' => 'Email should contain only latin symbols and numbers, Cyrillic prohibited.',
            ],
            'wallet' => [
                'regEx' => '#^(U)[0-9]{10,150}$#',
                'error' => 'Wallet should be match such mask U123456789, try again please.',
            ],
            'password' => [
                'regEx' => '#^[a-zA-Z0-9]{10,50}$#',
                'error' => 'Password should have at least 10 and no more 50 symbols, only numbers with latin letters.'
            ]
        ];

        foreach ($patternFields as $feild) {
            if (!isset ($inputFields[$feild]) or empty ($inputFields[$feild]) or !preg_match ($rules[$feild]['regEx'], $inputFields[$feild])) {
                $this->error = $rules[$feild]['error'];
                
                return false;
            }
        }

        return true;
    }

    /**
     * Checking method, take e-mail string and return a bool value.
     * 
     * @param string $email
     * @return bool
     */
    public function checkOccupationEmail (string $email): bool {
        if (!is_bool ($this->query ('SELECT id FROM users WHERE email = :email', ['email' => $email], 'fetchColumn'))) {
            $this->error = 'Email is busy.';

            return false;
        } else
            return true;
    }

    /**
     * Checking method, take login string and return a bool value.
     * 
     * @param string $login
     * @return bool
     */
    public function checkOccupationLogin (string $login): bool {
        if (!is_bool ($this->query ('SELECT id FROM users WHERE login = :login', ['login' => $login], 'fetchColumn'))) {
            $this->error = 'Login is busy.';

            return false;
        } else
            return true;
    }

    /**
     * Service method, creating token for confirm e-mail address.
     * Returned string is 30-chars length.
     * 
     * @param void
     * @return string
     */
    public function createToken (): string {
        return substr (str_shuffle (str_repeat ('1234567890qwertyuiopasdfghjklzxcvbnm', 5)), 0, 30);
    }

    /**
     * Service method, checking confirmation of profile by token from e-mail.
     * 
     * @param string $token
     * @return bool
     */
    public function checkToken (string $token): bool {
        if (!is_bool ($this->query('SELECT id FROM users WHERE token = :token', ['token' => $token], 'fetchColumn')))
            return true;
        else
            return false;
    }

    /**
     * Service method, which activate user account. 
     * Standard query change int value of status into db.
     * 
     * @param void
     * @return void
     */
    public function activate (string $token): void {
        $this->query ('UPDATE users SET status = 1, token = "" WHERE token = :token', ['token' => $token]);
    }

    /**
     * Registration method, combine the query into db for saving user data and sending confirmation e-mail.
     * Method taking parameter is $_POST array, named as $inputFields.
     * 
     * @param array $inputFields
     * @return bool
     */
    public function register (array $inputFields): bool {
        $token = $this->createToken ();
        
        $params = [
            'id'        =>  null,
            'login'     =>  $inputFields['login'],
            'email'     =>  $inputFields['email'],
            'wallet'    =>  $inputFields['wallet'],
            'password'  =>  password_hash ($inputFields['password'], PASSWORD_DEFAULT),
            'token'     =>  $token,
            'status'    =>  0,
            'access'    =>  'USER'
        ];

        try {
            $this->query ("INSERT INTO users VALUES (:id, :login, :email, :password, :wallet, :token, :status, :access)", $params);
        }catch (\PDOException $e) {
            exit ($e->getMessage ());
        }
        
        if (mail ($params['email'], "Confirmation email from {$_SERVER['HTTP_HOST']}", "Confirm: http://{$_SERVER['HTTP_HOST']}/user/confirm?token={$token}"))
            return true;
        else 
            return false;
    }

    /**
     * Checking user input data, which consist from login and password.
     * 
     * @param string $login
     * @param string $password
     * @return bool
     */
    public function checkUserData (string $login, string $password): bool {
        $hashPassword = $this->query ('SELECT password FROM users WHERE login = :login', ['login' => $login], 'fetchColumn');
        
        if (!is_bool ($hashPassword) and password_verify ($password, $hashPassword)) 
            return true;
        else
            return false;
    }

    /**
     * Universal method, will use into login-action and confirm-action of UserController.
     * Params, it's a $field, which has name of the field in db and target value, which the field should contain.
     * 
     * @param string $field
     * @param string $login
     * @return bool
     */
    public function checkUserStatusByField (string $field, string $fieldValue): bool {
        $status = $this->query ('SELECT status FROM users WHERE '.$field.' = :'.$field, [$field => $fieldValue], 'fetchColumn');

        if (!is_bool ($status) and $status == "1" or $status == 1)
            return true;
        else
            return false;
    }

    public function login (string $login): bool {
        $userAccess = $this->query ('SELECT access FROM users WHERE login = :login', ['login' => $login], 'fetchColumn');

        if (!is_bool ($userAccess)) {
            $_SESSION['SESSIONDATA'][$userAccess] = hash ('crc32b', $userAccess).hash ('adler32', $userAccess);
            $_SESSION['SESSIONDATA']['login'] = $login;
            return true;
        } else 
            return false;
    }
}