<?php 

namespace app\src\models;

use app\src\models\Model;

class PostModel extends Model {

    /**
     * Example
     */
    public function getRowById (int $n) {
        return  $this->findBy ('test', ['id' => $n]);
    }

    /**
     * Example
     */
    public function getRowByName (string $name) {
        return $this->findBy ('test', ['name' => $name]);
    }
}