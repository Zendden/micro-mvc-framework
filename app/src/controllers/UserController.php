<?php 

namespace app\src\controllers;

use app\src\models\UserModel;

class UserController extends Controller {

    /**
     * Static propertie $role: Should be implemented in each controller.
     * 
     * @var string $role
     */
    private static $role = self::GUEST;

    /**
     * Model handler
     * 
     * @var UserModel $model
     */
    protected $model;


    public function __construct () {
        /* Call parent constructor of Controller */
        parent::__construct ();
        /* Implementing user-model */
        $this->model = new UserModel ();
    }


    public function index (): void {
        echo 'Here should be a profile.';
    }

    public function login (): void {

        if (!empty ($_POST)) {
            $requiredFields = ['login', 'password'];

            if (!$this->model->validateUserInput ($requiredFields, $_POST)) {
                $this->jsonResponse ('error', $this->model->error);

            } elseif (!$this->model->checkUserData ($_POST['login'], $_POST['password'])) {
                $this->jsonResponse ('error', 'Passed wrong user data.');
                
            } elseif (!$this->model->checkUserStatusByField ('login', $_POST['login'])) {
                $this->jsonResponse ('error', 'Your account wait confirmation by e-mail.');
            }
           
            if ($this->model->login ($_POST['login'])) $this->jsonRedirect ('http://mvcframework/main');
        }

        $this->render (['title' => 'Login page']);
    }

    public function logout (): void {
        unset ($_SESSION['SESSIONDATA']);

        $_SESSION['SESSIONDATA'][self::GUEST] = hash ('crc32b', self::GUEST).hash ('adler32', self::GUEST);
        $_SESSION['SESSIONDATA']['login'] = self::GUEST;

        $this->httpRedirect ('user/login');
    }

    public function register (): void {

        if (!empty ($_POST)) {
            $requiredFields = ['login', 'email', 'wallet', 'password'];

            if (!$this->model->validateUserInput ($requiredFields, $_POST)) {
                $this->jsonResponse ('error', $this->model->error);

            } elseif (!$this->model->checkOccupationEmail ($_POST['email'])) {
                $this->jsonResponse ('error', $this->model->error);

            } elseif (!$this->model->checkOccupationLogin ($_POST['login'])) {
                $this->jsonResponse ('error', $this->model->error);
            }

            $this->model->register ($_POST);
            $this->jsonResponse ('Success', 'Success. Check e-mail ['.$_POST['email'].'] for complete your registration.');
        }

        $this->render (['title' => 'Registration page']);
    }

    public function confirm (): void {
        $token = addslashes (htmlspecialchars ($_GET['token']));

        if ($this->model->checkToken ($token ?? die ('Token is not exist!')))
            $this->model->activate ($token);
        else
            $this->httpRedirect ($_SERVER['HTTP_HOST']);

        $this->render (['title' => 'Confirmation account page']);
    }

    /**
     * Required access method.
     * 
     * @param void
     * @return string 
     */
    public static function getRole (): string {
        return self::$role;
    } 
}