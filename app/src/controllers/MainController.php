<?php

namespace app\src\controllers;

use app\src\controllers\Controller;
use app\core\Database;

class MainController extends Controller {

    /**
     * Access propertie.
     */
    private static $role = self::GUEST;

    /**
     * Default index method.
     */
    public function index () {
        $this->render (['name' => 'Jhon Doe', 'title' => 'Index page']);
    }

    public function showFromDb () {
        $connection = Database::getConnection ();

        foreach($connection->query('SELECT * FROM test') as $row) {
            $result[] = [$row['id'], $row['name']];
        }
 
        $this->render->setAnotherLayout ('main', 'index');
        $this->render (['name' => 'Jhon Doe', 'title' => 'Index page', 'exampleResult' => $result]);
    }

    /**
     * Example
     */
    public function show ($params = []) {
        $this->render->setAnotherLayout ('main', 'index');
        $this->render (['exampleResult' => $params, 'title' => 'Show page']);
    }

    /**
     * Getter of access propertie.
     */
    public static function getRole (): string {
        return self::$role;
    }

}