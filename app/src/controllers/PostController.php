<?php

namespace app\src\controllers;

use app\core\Render;
use app\src\models\PostModel;

class PostController extends Controller {

    /**
     * Static propertie $role: Should be implemented in each controller.
     * 
     * @var string $role
     */
    private static $role = self::GUEST;

    /**
     * Model handler 
     * 
     * @var PostModel $model
     */
    protected $model;


    public function __construct () {
        /**
         * Controller, from which extanded have own constructor.
         * If you want use construcot of child-controllers, so you need call a parent::__constructor.
         */
        parent::__construct ();

        /**
         * Implementing model
         * 
         * @var PostModel $model
         */
        $this->model = new PostModel ();
    }


    /**
     * Default method of each controller. Can't having params.
     */
    public function index () {
        echo "Index post page";
    }

    /**
     * Example method
     */
    public function show () {
        echo "Show post page";
    }

    /**
     * Example
     */
    public function go () {
        echo "Go post page";

        /**
         * Method which take controllerName/folderName and methodName/firstPartNameOfLayout.
         * Example: PostController and PostController->showAllPosts, 
         *          will be of this kind -> $this->setAnotherLayout ('post', 'showAllPosts');
         */
        $this->render->setAnotherLayout ('main', 'show');

        $this->render ();
    }

    /**
     * Must be implemented into each controller.
     * 
     * @param void
     * @return string
     */
    public static function getRole (): string {
        return self::$role;
    }
}