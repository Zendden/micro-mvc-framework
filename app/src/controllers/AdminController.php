<?php

namespace app\src\controllers;

use app\core\Render;

class AdminController extends Controller {

    /**
     * Static propertie $role: Should be implemented in each controller.
     * 
     * @var string $role
     */
    private static $role = self::ADMIN;


    /**
     * Default method of each controller. Can't having params.
     */
    public function index () {
       //Example method for rendering, etc. 

       /**
        * Method render does not need any params.
        * Layout defines by automatically if file with tempalte exist.
        */
        $this->render->setAnotherLayout ('main', 'index');
        $this->render (['name' => 'Clark', 'exampleResult' => 'Tempalte of MainController']); //Template of this class is not exist. Output: template not found
    }

    /**
     * Example method
     */
    public function show () {
        $this->render->setAnotherLayout ('main', 'show');
        $this->render ();
    }

    /**
     * Example
     */
    public function go () {
        /**
         * Method which take controllerName/folderName and methodName/firstPartNameOfLayout.
         * Example: PostController and PostController->showAllPosts, 
         *          will be of this kind -> $this->setAnotherLayout ('post', 'showAllPosts');
         */
        $this->render->setAnotherLayout ('main', 'show');

        /**
         * Parameter of this method should contain array with ['varName' => 'varValue'],
         * which will be passed into template. 
         * 
         * @param array|null
         */
        $this->render (['exampleResult' => $result, 'name' => 'Василий-Экземплович']);
    }

    /**
     * Must be implemented into each controller.
     * 
     * @param void
     * @return string
     */
    public static function getRole (): string {
        return self::$role;
    }
}