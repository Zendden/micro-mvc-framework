<?php

namespace app\src\controllers;

use app\core\Render;

/**
 * In each controller must be propertie = `private static $role = self::GUEST|USER|ADMIN`
 */
abstract class Controller {

    /**
     * Roles for access control and clarifing Routes visibility.
     */
    const USER          = 'USER';
    const GUEST         = 'GUEST';
    const ADMIN         = 'ADMIN';
    const SUPERADMIN    = 'SUPERADMIN';

    /**
     * Instance handler of Render class.
     * 
     * @var \Render|null $render
     */
    protected $render = null;


    /**
     * Default constructor
     * 
     * @param void
     */
    public function __construct () {
        $this->render = new Render ();
    }


    /**
     * Abstract method `index ()`, which should be described later.
     */
    abstract public function index ();

    /**
     * Abstract static method `getRole ()`, must return the Static Role of current controller.
     * In each controller must be propertie = `private static $role = self::GUEST|USER|ADMIN`
     */
    abstract public static function getRole ();


    /**
     * Method-adapter for short-using render method.
     * 
     * @param array $param
     * @return void
     */
    protected function render (array $param = []): void {
       $this->render->render ($param);
    }

    /**
     * Service method for prepare link before redirect.
     * 
     * @param string $url
     * @return string
     */
    public static function prepareUrl (string $url): string {
        $validUrl = false;

        /**
         * Checking the correct type of URL for redirect, 
         * if necessary, then concatenate the protocol
         * and cutting extra slashes.
         */
        if ((preg_match ("[(http://{$_SERVER['HTTP_HOST']})]", $url, $matches)) === 1) {
            $validUrl = filter_var ($url, FILTER_VALIDATE_URL);

        } elseif (preg_match ("[(https://)]", $url, $matches) === 1) {
            $validUrl = filter_var (preg_replace ("[(https://)]", "http://", $url), FILTER_VALIDATE_URL);
            
        } elseif ((preg_match ("[({$_SERVER['HTTP_HOST']}/)|({$_SERVER['HTTP_HOST']})]", $url, $matches)) === 1) {
            $validUrl = filter_var ("http://$url", FILTER_VALIDATE_URL);

        } else {
            $validUrl = filter_var ("http://{$_SERVER['HTTP_HOST']}/".ltrim ($url, '/'), FILTER_VALIDATE_URL);
        }

        if (!is_bool ($validUrl)) {
            return $validUrl;
        } else {
            throw new Exception (
                'Bad request, url uncorrect.',
                500
            );
        }
    }

    /**
     * This method take any type url and redirected with help of HTTP headers.
     * 
     * @param string $url
     * @return void
     */
    public function httpRedirect (string $url): void {
        header (
            "Location: {$this->prepareUrl ($url)}"
        );
    }

    /**
     * This method also take any url and redirect with help of JavaScript (script location - (form.js)).
     * 
     * @param string $url
     * @param string
     */
    public function jsonRedirect (string $url): string {
        exit (
            json_encode ([
                'url' => $this->prepareUrl ($url)
            ])
        );
    }

    /**
     * Method, which allow to stop current script and set the status of exit - function.
     * Set response for AJAX query.
     * 
     * @param string $status
     * @param string $message
     * @return string
     */
    public function jsonResponse (string $status, string $message): string {
        exit (json_encode (['status' => strtolower ($status) ?? '', 'message' => $message ?? '']));
    }

    /**
     * Method, which control user access to any module.
     * 
     * @param string $role
     * @return bool
     */
    public static function checkAccess (string $role): bool {
        $access = false;

        if (isset ($_SESSION['SESSIONDATA'])) {
            /**
             * @var string $role    | Role, which passed from controller
             * @var array $roles    | Roles-list for check availability $role from controller into $roles array
             */
            $role   = strtoupper ($role);
            $roles  = require ('app\config\access.conf.php'); //Requiring array with roles-list ['GUEST','USER','ADMIN'];

            /**
             * Checking user session array for availability the Role and the secret-key variables.
             */
            if (in_array ($role, $roles)) {
                foreach ($roles as $userRole => $roleWeight) {
                    if (array_key_exists ($userRole, $_SESSION['SESSIONDATA'])) {
                        /**
                         * If user-role have more weight than controller-role, then access granted.
                         */
                        if ($roles[$userRole] >= $roles[$role]) {
                            $access = true;
                        }
                    }
                }
            } else 
                throw new Exception (
                    'Access error, such role is not exist. Please, contact with administrator.',
                    403
                );
        } else {
            $_SESSION['SESSIONDATA'][self::GUEST] = hash ('crc32b', self::GUEST).hash ('adler32', self::GUEST);
            $_SESSION['SESSIONDATA']['login'] = self::GUEST;
        }

        return $access;
    }
}