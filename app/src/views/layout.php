<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="theme-color" content="#563d7c">

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous">
        <!-- CSS -->
        <link rel="stylesheet" href="/../../public/css/style.css" crossorigin="anonymous">
        <!-- jQUERY -->
        <script src="/../../public/js/jquery.js" crossorigin="anonymous"></script>

        <!-- TITLE -->
        <title>
            <?= $title ? $title : 'micro-MVC framework' ?>
        </title>
    </head>
    <body class="body">

        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">

                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="http://mvcframework/main">Home</a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Posts</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown01">
                            <a class="dropdown-item" href="/post">Main</a>
                            <a class="dropdown-item" href="/post/show">Show</a>
                            <a class="dropdown-item" href="/post/go">Go</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown02">
                            <a class="dropdown-item" href="/admin">Main</a>
                            <a class="dropdown-item" href="/admin/show">Show</a>
                            <a class="dropdown-item" href="/admin/go">Go</a>
                        </div>
                    </li>


                    <?php if (isset ($_SESSION['SESSIONDATA']) and $_SESSION['SESSIONDATA']['login'] !== 'GUEST'): ?>

                        <li class="nav-item">
                            <a class="nav-link" href="http://mvcframework/user/logout" tabindex="-1" aria-disabled="false">Logout</a>
                        </li>

                    <?php else: ?>

                        <li class="nav-item">
                            <a class="nav-link" href="http://mvcframework/user/login" tabindex="-1" aria-disabled="false">Login</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="http://mvcframework/user/register" tabindex="-1" aria-disabled="false">Register</a>
                        </li>

                    <?php endif; ?>


                </ul>

                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>

            </div>
        </nav>

        <main role="main">

            <!-- Main jumbotron for a primary marketing message or call to action -->
            <div class="jumbotron">
                <div class="container">
                    <h1 class="display-4" align="center">WebSite based on my MVC framework</h1>
                    <hr>
                    <h2 class="lead" align="center">This site is prototype of main project realised as example. The foundation of HTML-template had take a bootstrap `example`.</h2>
                </div>
            </div>

            <!-- Main space for content -->
            <div class="container">
                <section>
                    <?= $content ? $content : ''; ?>
                </section>
            </div>

        </main>

        <footer class="footer-container">
            <div class="lead">
                <p class="footer-content">
                    &copy; GitLab: @Zendden
                </p>
            </div>
        </footer>
       
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
        <script src="/../../public/js/scripts.js" crossorigin="anonymous"></script>
    </body>
</html>