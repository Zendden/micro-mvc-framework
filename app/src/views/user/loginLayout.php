
<!-- Custom styles for this template -->
<link href="/../../../public/css/login.css" rel="stylesheet">

<form class="form-signin" action="/user/login" method="POST" id="form">

    <div align="center">
        <img class="mb-4" src="/../../../public/img/icons/loginIcon.png" alt="" width="300" height="300">
    </div>

    <label for="inputLogin" class="sr-only">Login</label>
    <input type="text" id="inputLogin" class="form-control" placeholder="Login" name="login" required autofocus>

    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>

    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

</form>

<script src="/../../public/js/form.js" crossorigin="anonymous"></script>