<!-- Custom styles for this template -->
<link href="/../../../public/css/register.css" rel="stylesheet">

<form class="form-register" action="/user/register" method="POST" id="form">

    <div align="center">
        <img class="mb-4" src="/../../../public/img/icons/registrationIcon.png" alt="" width="300" height="300">
    </div>

    <label for="inputLogin" class="sr-only">Nickname</label>
    <input type="text" id="inputLogin" class="form-control" placeholder="Login" name="login" required autofocus>

    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="email" required>

    <label for="inputWallet" class="sr-only">Wallet</label>
    <input type="text" id="inputWallet" class="form-control" placeholder="Perfect money address" name="wallet"
        onmouseover="
            if (this.value == '') {
                this.value = 'U';
            } 
        " onmouseout="
            if (this.value == '' || this.value == 'U') {
                this.value = '';
            } 
        "  required>
    
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>

    <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit" value="sent">Register</button>

</form>

<script src="/../../public/js/form.js" crossorigin="anonymous"></script>