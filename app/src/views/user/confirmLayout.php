<!-- Custom styles for this template -->
<link href="/../../../public/css/register.css" rel="stylesheet">

<div align='center'>
    <h1>Succefully activated profile!</h1><br>
    <h3>You will be automatically redirected to the LogIn page...</h3>
</div>

<script>
    $(document).ready(function() {
        function redirect () {
            window.location.href = "http://mvcframework/user/login";
        }

        setTimeout(redirect, 5000)
    });
</script>
