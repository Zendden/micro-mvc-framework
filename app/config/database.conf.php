<?php

    return [
        'host'  =>      'localhost',    //Host for connection to DB, default `localhost`
        'user'  =>      'root',         //Username, which you use for entry into MySQL
        'name'  =>      'mvctest',      //Database name, target DB, which you want to use
        'pass'  =>      ''              //Personal password for entry into MySQL
    ];