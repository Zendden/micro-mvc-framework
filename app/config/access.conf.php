<?php 

    return [
        'GUEST'         =>  0,
        'USER'          =>  1,
        'ADMIN'         =>  2,
        'SUPERADMIN'    =>  3
    ];