<?php

    return [
        'defaultController'         =>  'Main',                         //   example: {Main}
        'defaultAction'             =>  'index',                        //   example: {index}
        'pathPrefix'                =>  "app\src\controllers\\",        //   example: {app\src\controllers\\}
        'namingPostfix'             =>  'Controller',                   //   example: {Controller}
        'defaultFileExtention'      =>  '.php'                          //   example: {.php}
    ];