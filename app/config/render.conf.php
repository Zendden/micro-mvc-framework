<?php

    return [
        'defaultController'         =>  'Main',                 //      example: {Main}
        'defaultAction'             =>  'index',                //      example: {index}
        'defaultLayout'             =>  'layout',               //      example: {layout}
        'pathPrefix'                =>  "app\src\\views",       //      example: {app\src\\views}
        'namingPostfix'             =>  'Layout',               //      example: {Layout}
        'defaultFileExtention'      =>  '.php'                  //      example: {.php}
    ];