<?php

namespace app;

/**
 * Starting session.
 */
session_start ();


/**
 * Startup the autoloading of classes.
 */
require_once ('app\core\Autolaod.php');


/**
 * Typing of required namespaces.
 */
use app\core\Router;

/**
 * $uriParts    - contains the names of Controller and Action of target-controller.
 * $params      - contains an array with GET-parameters of query.
 * $routerConf  - contains a necessary options for configure Router.
 */
$uriParts   = Router::getUriParts ();
$params     = Router::getParams ();
$routerConf = require_once ('app\config\router.conf.php');

/**
 * $targetController = \app\src\controllers\{ControllerTitle}
 * Full path = __DIR__.$targetController.'.php'
 */
$targetController = !empty ($uriParts[0]) 
    ? $routerConf['pathPrefix'].ucfirst ($uriParts[0]).$routerConf['namingPostfix']
    : $routerConf['pathPrefix'].$routerConf['defaultController'].$routerConf['namingPostfix'];

$targetAction     = !empty ($uriParts[1])
    ? strtolower ($uriParts[1])
    : $routerConf['defaultAction'];

/**
 * Handling exceptions.
 */
try {
    if (!class_exists ($targetController)) {
        throw new \Exception (
            'Class ['.$targetController.'] not found.', 
            500
        );
    } else {
        /**
         * This `checkAccess` method checks access to the route.
         * Return: true or false, if access denied.
         * 
         * @var bool $access
         */
        $access = $targetController::checkAccess ($targetController::getRole ());

        if ($access) {
            /**
             * If $access is granted, so create a new instance of target controller Class.
             * 
             * @var app\src\controllers\Controller $controller
             */
            $controller = new $targetController ();

            /**
             * If method exist, so call target method/route.
             */
            if (method_exists ($controller, $targetAction)) {
                $controller->$targetAction ($params ?? null);
            } else
                throw new \Exception (
                    'Method ['.$targetAction.'] of class ['.$targetController.'] is not exist.',
                    404
                );
        } else {
            throw new \Exception (
                'Page is not available',
                403
            );
        }
    }
} catch (\Exception $e) {
    if ($e->getCode () === 403) echo "<hr>Access denied: {$e->getMessage ()}<hr>";
    if ($e->getCode () === 404) echo "<hr>Not found: {$e->getMessage ()}<hr>";
    if ($e->getCode () === 500) echo "<hr>Internal server error: {$e->getMessage ()}<br>Please, contact with Administrator.<hr>";
} catch (\PDOException $e) {
    echo "<hr>Connection error: {$e->getMessage ()}<hr>";
}



