$(document).ready(function() {

    function resize() {
        $(".body").css({ "width": document.documentElement.clientWidth, "height": document.documentElement.clientHeight });
    }


    function get_scroll(a) {
        var d = document,
            b = d.body,
            e = d.documentElement,
            c = "client" + a;
        a = "scroll" + a;
        return /CSS/.test(d.compatMode) ? (e[c] < e[a]) : (b[c] < b[a])
    };

    window.addEventListener("resize", function() {
        if (get_scroll('Width')) {
            resize();
        }
    });
});