$(document).ready(function() {
    $('#form').submit(function(event) {
        event.preventDefault();

        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(response) {
                jsonResponse = JSON.parse(response);

                if (jsonResponse.url != undefined) {
                    window.location.href = jsonResponse.url;

                } else if (jsonResponse.status != undefined && jsonResponse.status == 'success') {
                    alert(jsonResponse.message);

                } else if (jsonResponse.status != undefined && jsonResponse.status == 'error') {
                    console.log(jsonResponse);

                } else if (jsonResponse.status != undefined) {
                    console.log('Bad JSON response.');
                }
            }
        });
    });
});